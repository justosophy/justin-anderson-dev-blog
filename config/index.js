module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"

  siteTitle: 'JustinAnderson.dev', // Navigation and Site Title
  siteTitleAlt: 'JustinAnderson.dev', // Alternative Site title for SEO
  siteTitleManifest: 'JustinAndersonDotDev',
  siteUrl: 'https://justinanderson.dev', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteHeadline: 'Writing and publishing content', // Headline for schema.org JSONLD
  siteBanner: '/social/banner.jpg', // Your image for og:image tag. You can find it in the /static folder
  favicon: 'src/favicon.png', // Your image for favicons. You can find it in the /src folder
  siteDescription: 'Blog by Justin Anderson.', // Your site description
  author: 'Justin Anderson', // Author for schemaORGJSONLD
  siteLogo: '/social/logo.png', // Image for schemaORGJSONLD

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@justosophy', // Twitter Username - Optional
  ogSiteName: 'Justin Anderson dot dev', // Facebook Site Name - Optional
  ogLanguage: 'en_US', // Facebook Language
  googleAnalyticsID: 'UA-140133119-1',

  // Manifest and Progress color
  // See: https://developers.google.com/web/fundamentals/web-app-manifest/
  themeColor: '#4A913C',
  backgroundColor: '#2b2e3c',
}
