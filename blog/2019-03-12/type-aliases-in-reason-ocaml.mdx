---
date: "2019-03-12"
title: "Type aliases and explicit types in ReasonML/OCaml"
slug: "type-aliases-in-reason-ocaml"
categories:
  - Development
  - Functional Programming
  - OCaml/ReasonML
---

> Should I use [type aliasing](https://reasonml.github.io/docs/en/type#aliases)? It seems to break my code a lot.

\- a peer asked me. They recently started trying out ReasonML from a JavaScript background.

The answer is often one of personal preference. [Higher performance programming](https://lepigre.fr/files/publications/CLS2019.pdf) concerns and library designers likely have different needs to someone beginning ML style languages. There is some conventional wisdom in ReasonML/OCaml to recommend other approaches over type aliasing in several circumstances.

## Type aliasing

ReasonML and OCaml allow you to specify named types that are aliases for other types.

```reason
/* ReasonML */
type score = int;
type name = string;

let myscore : score = 9000;
let myname : name = "Justin";
```

A difficulty with this is the compiler can seem too eager in detecting type aliases. If my next line were `let age = 33` , then ReasonML/OCaml would infer the type of `age` with the type alias `score`.

```reason
/* ReasonML */
/* .. */
let age = 33; /* <- infers type of 33 as with alias "score" */
```

Without explicit annotation the type of a value binding is not always obvious. Yet common style in ReasonML/OCaml it to omit annotation, preferring the _terms over types_ approach much of the time.

## Record type aliasing

Record types appear different to simpler type aliasing but are still a form of type aliasing. They can produce similar unexpected consequences with type inference:

### Comparable aliases

Unexpected inference of type aliasing can prevent code from compiling:

```reason
/* ReasonML */
type human = { name: string, age: int };
type unicorn = { name: string, age: int };

let person = { name: "Justin", age: 99 };

let getHumanAge = (h:human) => h.age;

let age = getHumanAge(person);

```

Produces the error:

```markdown
This has type:
  `unicorn`
But somewhere wanted:
  `human`
```

The compiler inferred the `person` binding to be of type `unicorn`, when the `getHumanAge` function required a record type of `human`. This could have been resolved by removing the "`let unicorn`" line, or by moving it anywhere below the "`let person`" binding, or by explicit annotation:

```reason
/* ReasonML */
let person : human = { name: "Justin", age: 99 };
```

This kind of fragility regarding order of declaration can leave a bad smell for an expanding codebase.

### Comparable but different aliases

If we modify one of the fields of a record type to appear different, we still get a compilation error:

```reason
/* ReasonML */
type teacher = { name: string, age: int };
type student = { name: string, age: string };

let person = { name: "Justin", age: 99 };
```

Giving the error:

```markdown
This has type:
  `int`
But somewhere wanted:
  `string`
```

The compiler does not determine the `age` field as matching the `teacher` record type. The `student` type alias was expected in scope for these record fields in the literal value.

We might imagine the compiler could be improved to make these determinations. There is however no way be certain what the programmer _really meant_. Did they mean the `teacher` type by using an integer, or did they mean the `student` type and gave an integer instead of a string as a simple typo? In such a scenario, should the compiler return an error or not.

## Being explicit with constructor values

### Variants

Instead of type aliasing, it is a common approach to use an explicit [variant type](https://reasonml.github.io/docs/en/variant) with a single constructor. Sometimes referred to as _boxing_.

Instead of using `type score = int` we might use the following:

```ocaml
(* OCaml *)
type score = Score of int
let myscore = Score 9000
```

```reason
/* ReasonML */
type score = | Score(int);
let myscore = Score(9000);
```

Units of measure are good candidates for this approach. Consider modelling temperature,

```reason
/* ReasonML */
type temperature = float;
let mytemp : temperature = 21.4;
```

vs.

```reason
/* ReasonML */
type temperature = | Temperature(float);
let mytemp = Temperature(21.4);
```

In the second example `mytemp` is not annotated since the `Temperature` constructor allows the compiler to infer the `temperature` type explicitly.

Variant types with constructor values can allow for more expressive and explicit modelling of our problem compared with simpler type aliasing. We could then expand on our temperature type values:

```reason
/* ReasonML */
type temperature =
  | Celcius(float)
  | Fahrenheit(float)
  | Kelvin(float);

let mytemp = Celcius(21.4);
let alsotemp = Farrenheit(70.52);
```

### Variants and records

Records can be used with this approach to inform the type inference checker, without explicit type annotation. So how do we place records into named constructor values? We might have expected ReasonML to define a literal syntax like so:

```reason
/* invalid syntax */
type human = Human({name: string, age: int })
```

But this embedded syntax is not possible as records really are separate type aliases.

One approach is to refer to a secondary type definition within the scope of the main type definition.

```reason
/* ReasonML */
type _human = { name: string, age: int };
type human = | Human(_human);

type _unicorn = { name: string, age: string };
type unicorn = | Unicorn(_unicorn);

let person = Human{name: "Justin", age: 99};
let bob = Unicorn{name: "Bob", age: "one bajillion"};
```

Is this approach now any better? Instead of two type definitions before we have four. Whereas the text `human` would have been on the left-hand side with type annotation we have `Human` on the right-hand side with the constructor.

One thing we _can_ do differently now is move `person` and `bob` around in the code _without annotating_. The type inference no longer gets confused by the different record fields, as the `ConstructorName{...}` in front provides explicit information as to the type.

We could tidy things up by using the `and` keyword:

```reason
/* ReasonML */
type human = | Human(_human)
  and _human = { name: string, age: int };
type unicorn = | Unicorn(_unicorn)
  and _unicorn = { name: string, age: string };

let person = Human{name: "Justin", age: 99};
let bob = Unicorn{name: "Bob", age: "one bajillion"};
```

## Model the problem as best as you can

I did say personal preference plays a part here. If "type aliasing breaks my code" is an issue, then use more explicit types instead.

Record types are a common go to for developers starting ML languages from languages like JavaScript or Java. They more closely resemble objects and classes in more typical object oriented development and modelling.

ReasonML/OCaml and the like offer a rich and expressive type system to model and express problem domains, and records tend to be deemphasized as a primay approach.

Perhaps like temperate, our boxed values can be combined or extended to model complexity while not worrying about incorrect type inference.

We might extend the tale by addding a "`Werewolf`" creature, who once bitten retains some humanity until the full moon transforms then and erases it to nothing.

```reason
/* ReasonML */
type creature =
  | Human(_human)
  | Unicorn(_unicorn)
  | Werewolf(_werewolf)
    and _human = { name: string, age: int }
    and _unicorn = { name: string, canfly: bool }
    and _werewolf = { humanity: option(_human) };

let susan = Human{name: "Susan", age: 30};
let john = Werewolf{ humanity: Some{name: "John", age: 26} };

let fullmoon = (c:creature) => switch (c) {
  | Werewolf(_) => Werewolf{ humanity: None }
  | others => others
  };

let nameless = fullmoon(john);
```

Or we can extend a database table, a form submission, you decide. Whatever flies.
