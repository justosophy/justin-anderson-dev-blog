import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  text-align: center;
  svg {
    width: 10rem;
  }
  @media (max-width: ${props => props.theme.breakpoints.tablet}) {
    svg {
      width: 8rem;
    }
  }
  @media (max-width: ${props => props.theme.breakpoints.phone}) {
    svg {
      width: 8rem;
    }
  }
  .word-curve {
    stroke-dasharray: 4000;
    stroke-dashoffset: 4000;

    animation-duration: 2.8s;
    animation-timing-function: ease-in-out;
    animation-delay: 0.15s;
    animation-iteration-count: 1;
    animation-fill-mode: forwards;
    animation-name: dash;
  }
  @keyframes dash {
    from {
      stroke-dashoffset: 4000;
    }
    to {
      stroke-dashoffset: 0;
    }
  }
  .fill-curve {
    opacity: 0;
    animation-duration: 0.6s;
    animation-timing-function: ease-in-out;
    animation-delay: 1.3s;
    animation-iteration-count: 1;
    animation-fill-mode: forwards;
    animation-name: fillout;
  }
  @keyframes fillout {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`

const WaveLogo = () => (
  <Wrapper>
    <svg viewBox="0 0 1000 1000" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <title>Wave form image with the letters JA</title>
      <g id="Page-8" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g
          id="Group"
          transform="translate(20.000000, 135.000000)"
          stroke="rgba(0,0,0,0.9)"
          strokeLinejoin="round"
          strokeWidth="12"
        >
          <path
            className="a-curve fill-curve"
            d="M0,434 L448.084559,434 C560.500987,144.666667 648.264278,0 711.374431,0 C774.484584,0 857.359773,144.666667 960,434 L0,434 Z"
            fill="#DDDDDD"
          />
          <path
            className="a-curve word-curve"
            d="M0,434 L448.084559,434 C560.500987,144.666667 648.264278,0 711.374431,0 C774.484584,0 857.359773,144.666667 960,434 L0,434 Z"
          />
          <path
            className="j-curve word-curve"
            d="M0,730 L447.721581,730 C560.046945,440.666667 647.739141,296 710.798171,296 C773.8572,296 839.591144,394.887149 908,592.661447"
            transform="translate(454.000000, 513.000000) scale(-1, -1) translate(-454.000000, -513.000000) "
          />
          <path
            className="j-curve fill-curve"
            d="M403.625558,433.338553 L460.278419,296 C347.953055,585.333333 260.260859,730 197.201829,730 C134.1428,730 68.4088565,631.112851 0,433.338553 L403.625558,433.338553 Z"
            fill="#DDDDDD"
          />
        </g>
      </g>
    </svg>
  </Wrapper>
)

export default WaveLogo
