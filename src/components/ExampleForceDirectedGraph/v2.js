import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { forceSimulation, forceManyBody, forceCenter, forceLink, forceCollide } from 'd3-force'
import { interpolateCubehelixDefault } from 'd3-scale-chromatic'
// import LesMisData from './lesmisdata'
import SampleData from './sampledata'

import Button from '../Button'

const SVG_BACKGROUND = '#f9f9f9'

const Wrapper = styled.div`
  svg {
    display: block;
    background-color: ${SVG_BACKGROUND};
    margin: 0rem auto;
    max-width: 800px;
  }
  svg text {
    // font-size: 0.5rem;
    paint-order: stroke;
    stroke-linecap: butt;
    stroke-linejoin: miter;
  }
  svg circle {
    transition: cx 1s, cy 1s;
  }
  svg .node-text-group {
    transition: transform 1s;
  }
  svg line {
    transition: x1 1s, x2 1s, y1 1s, y2 1s;
  }
  svg path {
    transition: d 1s;
  }
`

const WIDTH = 1000
const HEIGHT = 600
const STRENGTH_LOWER = 40
const STRENGTH_UPPER = 80

const NODE_RADIUS = 40

const random = (lower, upper) => lower + Math.random() * (upper - lower + 1)
const randomInt = (lower, upper) => Math.floor(random(lower, upper))

const forceBoundary = () => {
  let nodes = []
  const force = alpha => {
    nodes.forEach(n => {
      if (n.x > WIDTH) {
        n.x = WIDTH - NODE_RADIUS
        n.vx = -1 / alpha
      }
      if (n.x < 0) {
        n.x = NODE_RADIUS
        n.vx = 0.5 / alpha
      }
      if (n.y > HEIGHT) {
        n.y = HEIGHT - NODE_RADIUS
        n.vy = -0.5 / alpha
      }
      if (n.y < 0) {
        n.y = NODE_RADIUS
        n.vy = 0.5 / alpha
      }
    })
  }
  force.initialize = v => {
    nodes = v
  }
  return force
}

forceBoundary.initialize = nodes => forceBoundary.bind(nodes, nodes)

const ExampleForceDirectedGraphV2 = () => {
  const [dataset, updateDataSet] = useState(SampleData)
  const [delta, updateDelta] = useState([])
  const [strength, updateStrength] = useState(random(STRENGTH_LOWER, STRENGTH_UPPER))

  useEffect(() => {
    forceSimulation()
      .stop()
      .nodes(dataset.nodes)
      .force('charge', forceManyBody().strength(-strength))
      .force('link', forceLink(dataset.links).id(n => n.id))
      .force('center', forceCenter(WIDTH * 0.5, HEIGHT * 0.5))
      .force('collide', forceCollide(NODE_RADIUS * 3).strength(0.5))
      .force('boundary', forceBoundary())
      .tick(300)
    updateDataSet(v => Object.assign({}, v, { simulated: true }))
  }, [strength])

  useEffect(() => {
    if (delta.length === 0) {
      return
    }

    const nodes = dataset.nodes.slice()

    delta.forEach(d => {
      if (d.action === 'add-node') {
        nodes.push(d.data)
      }
    })

    forceSimulation()
      .stop()
      .nodes(nodes)
      .force('charge', forceManyBody().strength(-strength))
      .force('link', forceLink(dataset.links).id(n => n.id))
      .force('center', forceCenter(WIDTH * 0.5, HEIGHT * 0.5))
      .force('collide', forceCollide(NODE_RADIUS * 3).strength(0.5))
      .force('boundary', forceBoundary())
      .tick(300)
    // updateDataSet(v => Object.assign({}, v))
    updateDelta([])
  }, [delta])

  return (
    <Wrapper className="force-directed-example">
      <svg viewBox={`0 0 ${WIDTH} ${HEIGHT}`} xmlns="http://www.w3.org/2000/svg">
        <defs>
          <marker
            id="arrow"
            markerWidth="10"
            markerHeight="10"
            refX={NODE_RADIUS * 1.4}
            refY="3"
            orient="auto"
            markerUnits="strokeWidth"
          >
            <path d="M0,0 L0,6 L9,3 z" fill="#888" />
          </marker>
        </defs>
        {dataset.simulated &&
          dataset.links.map(l => (
            <g key={`link_path_group_(${l.source.id})-(${l.target.id})`}>
              <path
                id={`link_path_(${l.source.id})-(${l.target.id})`}
                fill="none"
                stroke="#999"
                strokeWidth={2}
                d={`M${l.source.x},${l.source.y} L${l.target.x},${l.target.y}`}
                markerEnd="url(#arrow)"
                markerMid="url(#arrow)"
              />
              <path
                id={`link_path_reverse_(${l.source.id})-(${l.target.id})`}
                fill="none"
                strokeWidth={0}
                d={`M${l.target.x},${l.target.y} L${l.source.x},${l.source.y}`}
              />
            </g>
          ))}

        {dataset.simulated &&
          dataset.nodes.map(n => {
            const adjustedColor = n.color * 0.9
            const color = interpolateCubehelixDefault(adjustedColor / 10)
            const textColor = adjustedColor > 5 ? 'rgba(0,0,0,0.95)' : 'rgba(255,255,255,0.95)'
            const text =
              typeof n.text !== 'string' ? null : (
                <g className="node-text-group" transform={`translate(${n.x},${n.y})`}>
                  <text x={0} y={0} textAnchor="middle" stroke={color} strokeWidth={8} dy=".3em" fill={textColor}>
                    {n.text}
                  </text>
                </g>
              )
            return (
              <g key={`node_${n.id}`}>
                <circle cx={n.x} cy={n.y} r={NODE_RADIUS + 4} fill={color} strokeWidth={0} />
                <circle
                  cx={n.x}
                  cy={n.y}
                  r={NODE_RADIUS}
                  key={n.id}
                  fill={color}
                  stroke={SVG_BACKGROUND}
                  strokeWidth={2}
                />
                {text}
              </g>
            )
          })}
        {dataset.simulated &&
          dataset.links.map(l => {
            if (typeof l.text !== 'string') {
              return null
            }
            const isReverse = l.target.x > l.source.x
            const reverseStr = isReverse ? '' : 'reverse_'
            return (
              <text
                // key={`#link_text_(${l.source.id})-(${l.target.id})`}
                fill="#444"
                strokeWidth={4}
                dy=".3em"
                stroke={SVG_BACKGROUND}
                key={`#link_path_text_${reverseStr}(${l.source.id})-(${l.target.id})`}
              >
                <textPath
                  href={`#link_path_${reverseStr}(${l.source.id})-(${l.target.id})`}
                  // spacing="auto"
                  startOffset="40%"
                  lengthAdjust="spacingAndGlyphs"
                >
                  {l.text}
                </textPath>
              </text>
            )
          })}
      </svg>
      <div>{`Simulated: ${dataset.simulated === true}`}</div>
      <Button big onClick={() => updateStrength(random(STRENGTH_LOWER, STRENGTH_UPPER))}>
        REWEIGHT
      </Button>
      <Button
        big
        onClick={() => {
          const id = `randomadd_${Math.random()}`
          updateDataSet(v =>
            Object.assign({}, v, {
              nodes: v.nodes.concat({
                id,
                group: 1,
                color: randomInt(0, 10),
                text: 'hello',
              }),
              links: v.links.concat({
                source: id,
                target: v.nodes[randomInt(0, v.nodes.length - 1)].id,
                value: 1,
                text: 'LIKES',
              }),
            })
          )
        }}
      >
        Add
      </Button>
    </Wrapper>
  )
}

export default ExampleForceDirectedGraphV2
