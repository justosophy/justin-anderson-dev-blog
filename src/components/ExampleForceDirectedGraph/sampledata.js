export default {
  nodes: [
    { id: 'a', group: 1, color: 8, fx: 100, fy: 200, text: 'Piano' },
    { id: 'b', group: 1, color: 3, text: 'John' },
    { id: 'c', group: 1, color: 3, text: 'Mary' },
    { id: 'd', group: 1, color: 7, text: 'Chocolate' },
    { id: 'e', group: 1, color: 9, fx: 900, fy: 200, text: 'Africa' },
  ],
  links: [
    { source: 'b', target: 'a', value: 1, text: 'LIKES' },
    { source: 'b', target: 'd', value: 1, text: 'LIKES' },
    { source: 'a', target: 'c', value: 1, text: '?' },
    { source: 'c', target: 'd', value: 1, text: 'LIKES' },
    { source: 'd', target: 'e', value: 1 },
  ],
}
