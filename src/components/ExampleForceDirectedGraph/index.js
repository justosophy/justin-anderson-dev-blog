import React from 'react'
import styled from 'styled-components'
import ForceDirectedGraph from '../ForceDirectedGraph'
import LesMisData from './lesmisdata'

import Button from '../Button'

const Wrapper = styled.div`
  .candlestick-example {
    width: 100%;

    .chart {
      width: 100%;
    }

    .dashed-example-line {
      stroke-dasharray: 2, 2;
    }
  }
`

export default class ExampleForceDirectedGraph extends React.Component {
  state = {
    strength: Math.random() * 60 - 30,
  }

  render() {
    const { strength } = this.state
    return (
      <Wrapper className="force-directed-example">
        <Button big onClick={() => this.setState({ strength: Math.random() * 60 - 30 })}>
          {' '}
          REWEIGHT{' '}
        </Button>
        <ForceDirectedGraph data={LesMisData} height={500} width={500} animation strength={strength} />
      </Wrapper>
    )
  }
}
