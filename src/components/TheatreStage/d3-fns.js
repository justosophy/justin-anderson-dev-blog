const updateAndEnterSelections = (update, enter, className, appendTag) => [
  enter.append(appendTag).attr('class', className),
  update.select(`.${className}`),
]

const alterUpdatesAndEnters = ([enter, update], fn) => {
  fn(update)
  fn(enter)
}

export { updateAndEnterSelections, alterUpdatesAndEnters }
