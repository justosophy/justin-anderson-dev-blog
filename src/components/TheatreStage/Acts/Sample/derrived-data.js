import reactElementToJSXString from 'react-element-to-jsx-string'

import data from './data'

import { offsetEndsByRadius } from '../../line-fns'

const derrivedData = data.map(scene => {
  if (!Array.isArray(scene.data.nodes) && typeof scene.data.nodes === 'object') {
    scene.data.nodes = Object.values(scene.data.nodes)
  }
  scene.data.links.forEach(l => {
    const source = scene.data.nodes.find(n => n.id === l.source)
    const target = scene.data.nodes.find(n => n.id === l.target)
    const [sourcePoint, targetPoint] = offsetEndsByRadius(source, target, 15)
    l.source = { ...sourcePoint, id: source.id }
    l.target = { ...targetPoint, id: target.id }
  })

  if (!Array.isArray(scene.data.nodePaths) && typeof scene.data.nodePaths === 'object') {
    scene.data.nodePaths = Object.values(scene.data.nodePaths)
  }
  scene.data.nodePaths.forEach(np => {
    np.nodes = np.nodes.map(id => scene.data.nodes.find(n => n.id === id))

    np.edges = np.nodes.reduce((acc, current, index, arr) => {
      if (index === 0) {
        return acc
      }
      const prev = arr[index - 1]
      const [prevPoint, currentPoint] = offsetEndsByRadius(prev, current, 22)
      acc.push({
        source: { x: prevPoint.x, y: prevPoint.y },
        target: { x: currentPoint.x, y: currentPoint.y },
      })
      return acc
    }, [])
  })

  return scene
})

const bakedData = derrivedData.map(scene => ({
  ...scene,
  narration: reactElementToJSXString(typeof scene.narration === 'function' ? scene.narration() : scene.narration, {
    tabStop: 1,
  }),
}))
const bakedString = JSON.stringify(bakedData, null, 4)
  .replace(/\\n/g, '')
  .replace(/"<>/g, '(<>')
  .replace(/<\/>"/g, '</>)')
  .replace(/"(name|narration|data)"/g, '$1')
  .replace(/\s+</g, ' <')
  .replace(/>\s+/g, '>')

console.log(bakedString)

export default derrivedData
