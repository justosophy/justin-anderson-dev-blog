import * as d3 from 'd3'
import renderNodes from '../../render-nodes'
import renderEdges from '../../render-edges'
import renderNodePaths from '../../render-node-paths'

const render = (svg, data) => {
  const svgDoc = d3.select(svg)

  renderEdges(svgDoc, data)
  renderNodes(svgDoc, data)
  renderNodePaths(svgDoc, data)
}

export { render, renderNodes, renderEdges }
