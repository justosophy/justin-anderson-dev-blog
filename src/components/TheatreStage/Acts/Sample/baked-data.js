import React from 'react'

const bakedData = [
  {
    name: 'Start',
    narration: (
      <>
        <p>
          Ipsum do est quis amet labore. Sunt veniam incididunt{' '}
          <span style={{ color: '#4A913C', fontWeight: 'bold' }}>John </span>mollit amet consectetur sunt proident ad ea
          in <span style={{ color: 'rgb(204, 165, 227)', fontWeight: 'bold' }}>piano </span>. Exercitation ut proident
          et adipisicing nisi laboris exercitation duis sunt excepteur.{' '}
        </p>
        <p>
          Eiusmod in veniam id commodo amet consectetur sit sint tempor. Incididunt aute eiusmod anim consectetur nulla
          labore sint enim cupidatat exercitation et elit incididunt labore. Incididunt deserunt dolore aliqua nostrud.{' '}
        </p>
      </>
    ),
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 3,
          text: 'John',
          x: 440,
          y: 384,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 8,
          text: 'piano',
          x: 656,
          y: 240,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 502.4037720753383,
            y: 342.3974852831078,
            id: 'JOHN',
          },
          target: {
            x: 593.5962279246618,
            y: 281.6025147168922,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Add a node',
    narration: (
      <>
        <p>
          Occaecat mollit eu sunt irure esse do. Fugiat quis anim Lorem sit sint reprehenderit nostrud ex Lorem deserunt
          ut. Officia ad et eu dolore irure duis aute amet nisi magna nulla tempor.Sit qui amet laborum duis. Dolore
          nostrud aute in fugiat non ex occaecat quis magna dolor elit. Officia aliqua proident enim duis eiusmod
          voluptate. Nostrud esse est cupidatat consectetur non anim aliquip cupidatat. Esse id aliqua aute ut sunt sit
          magna do.{' '}
        </p>
        <p>
          Occaecat mollit eu sunt irure esse do. Fugiat quis anim Lorem sit sint reprehenderit nostrud ex Lorem deserunt
          ut. Eu aliqua adipisicing velit enim. Enim aliqua cillum sit consectetur est ad qui cupidatat. Officia ad et
          eu dolore irure duis aute amet nisi magna nulla tempor.Sit qui amet laborum duis. Dolore nostrud aute in
          fugiat non ex occaecat quis magna dolor elit. Officia aliqua proident enim duis eiusmod voluptate. Nostrud
          esse est cupidatat consectetur non anim aliquip cupidatat. Esse id aliqua aute ut sunt sit magna do.{' '}
        </p>
        <p>
          Occaecat mollit eu sunt irure esse do. Fugiat quis anim Lorem sit sint reprehenderit nostrud ex Lorem deserunt
          ut. Eu aliqua adipisicing velit enim. Enim aliqua cillum sit consectetur est ad qui cupidatat. Officia ad et
          eu dolore irure duis aute amet nisi magna nulla tempor.Sit qui amet laborum duis. Dolore nostrud aute in
          fugiat non ex occaecat quis magna dolor elit. Officia aliqua proident enim duis eiusmod voluptate. Nostrud
          esse est cupidatat consectetur non anim aliquip cupidatat. Esse id aliqua aute ut sunt sit magna do.{' '}
        </p>
      </>
    ),
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 3,
          text: 'John',
          x: 440,
          y: 384,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 8,
          text: 'piano',
          x: 584,
          y: 168,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 3,
          text: 'Mary',
          x: 584,
          y: 600,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 481.6025147168922,
            y: 321.5962279246617,
            id: 'JOHN',
          },
          target: {
            x: 542.3974852831078,
            y: 230.40377207533825,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 481.6025147168922,
            y: 446.4037720753383,
            id: 'JOHN',
          },
          target: {
            x: 542.3974852831078,
            y: 537.5962279246618,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Connect four',
    narration: <>Irure id eiusmod exercitation ipsum dolore commodo duis.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 3,
          text: 'John',
          x: 296,
          y: 384,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 8,
          text: 'piano',
          x: 512,
          y: 168,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 3,
          text: 'Mary',
          x: 512,
          y: 600,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 5,
          text: 'France',
          x: 728,
          y: 384,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 349.03300858899104,
            y: 330.96699141100896,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 349.03300858899104,
            y: 437.03300858899104,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 546.966991411009,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 546.966991411009,
            id: 'MARY',
          },
          target: {
            x: 674.966991411009,
            y: 437.03300858899104,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          target: {
            x: 674.966991411009,
            y: 330.96699141100896,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Trace path',
    narration: <>Magna dolor cupidatat culpa anim Lorem elit culpa ex occaecat.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 3,
          text: 'John',
          x: 296,
          y: 384,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 8,
          text: 'piano',
          x: 512,
          y: 168,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 3,
          text: 'Mary',
          x: 512,
          y: 600,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 5,
          text: 'France',
          x: 728,
          y: 384,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 349.03300858899104,
            y: 330.96699141100896,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 349.03300858899104,
            y: 437.03300858899104,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 546.966991411009,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 546.966991411009,
            id: 'MARY',
          },
          target: {
            x: 674.966991411009,
            y: 437.03300858899104,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          target: {
            x: 674.966991411009,
            y: 330.96699141100896,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [
        {
          id: 'nodePath1',
          nodes: [
            {
              id: 'MARY',
              color: 3,
              text: 'Mary',
              x: 512,
              y: 600,
              radius: 60,
            },
            {
              id: 'JOHN',
              color: 3,
              text: 'John',
              x: 296,
              y: 384,
              radius: 60,
            },
            {
              id: 'PIANO',
              color: 8,
              text: 'piano',
              x: 512,
              y: 168,
              radius: 60,
            },
          ],
          color: 2,
          edges: [
            {
              source: {
                x: 454.0172439427031,
                y: 542.0172439427031,
              },
              target: {
                x: 353.9827560572969,
                y: 441.9827560572969,
              },
            },
            {
              source: {
                x: 353.9827560572969,
                y: 326.0172439427031,
              },
              target: {
                x: 454.0172439427031,
                y: 225.98275605729688,
              },
            },
          ],
        },
      ],
    },
  },
  {
    name: 'Overlap paths',
    narration: <>Magna dolor cupidatat culpa anim Lorem elit culpa ex occaecat.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 3,
          text: 'John',
          x: 296,
          y: 384,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 8,
          text: 'piano',
          x: 512,
          y: 168,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 3,
          text: 'Mary',
          x: 512,
          y: 600,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 5,
          text: 'France',
          x: 728,
          y: 384,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 349.03300858899104,
            y: 330.96699141100896,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 349.03300858899104,
            y: 437.03300858899104,
            id: 'JOHN',
          },
          target: {
            x: 458.96699141100896,
            y: 546.966991411009,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 546.966991411009,
            id: 'MARY',
          },
          target: {
            x: 674.966991411009,
            y: 437.03300858899104,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 565.033008588991,
            y: 221.03300858899107,
            id: 'PIANO',
          },
          target: {
            x: 674.966991411009,
            y: 330.96699141100896,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [
        {
          id: 'nodePath1',
          nodes: [
            {
              id: 'MARY',
              color: 3,
              text: 'Mary',
              x: 512,
              y: 600,
              radius: 60,
            },
            {
              id: 'JOHN',
              color: 3,
              text: 'John',
              x: 296,
              y: 384,
              radius: 60,
            },
            {
              id: 'PIANO',
              color: 8,
              text: 'piano',
              x: 512,
              y: 168,
              radius: 60,
            },
          ],
          color: 2,
          edges: [
            {
              source: {
                x: 454.0172439427031,
                y: 542.0172439427031,
              },
              target: {
                x: 353.9827560572969,
                y: 441.9827560572969,
              },
            },
            {
              source: {
                x: 353.9827560572969,
                y: 326.0172439427031,
              },
              target: {
                x: 454.0172439427031,
                y: 225.98275605729688,
              },
            },
          ],
        },
        {
          id: 'nodePath2',
          nodes: [
            {
              id: 'FRANCE',
              color: 5,
              text: 'France',
              x: 728,
              y: 384,
              radius: 60,
            },
            {
              id: 'PIANO',
              color: 8,
              text: 'piano',
              x: 512,
              y: 168,
              radius: 60,
            },
            {
              id: 'MARY',
              color: 3,
              text: 'Mary',
              x: 512,
              y: 600,
              radius: 60,
            },
          ],
          color: 6,
          edges: [
            {
              source: {
                x: 670.0172439427031,
                y: 326.0172439427031,
              },
              target: {
                x: 569.9827560572969,
                y: 225.98275605729688,
              },
            },
            {
              source: {
                x: 512,
                y: 250,
              },
              target: {
                x: 512,
                y: 518,
              },
            },
          ],
        },
      ],
    },
  },
  {
    name: 'Adjust nodes',
    narration: <>Tempor excepteur tempor amet mollit.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 296,
          y: 384,
          radius: 25,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 512,
          y: 168,
          radius: 70,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 512,
          y: 600,
          radius: 8,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 728,
          y: 384,
          radius: 90,
        },
      ],
      links: [
        {
          source: {
            x: 324.2842712474619,
            y: 355.7157287525381,
            id: 'JOHN',
          },
          target: {
            x: 451.8959235991435,
            y: 228.1040764008565,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 324.2842712474619,
            y: 412.2842712474619,
            id: 'JOHN',
          },
          target: {
            x: 495.7365440327094,
            y: 583.7365440327094,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 528.2634559672906,
            y: 583.7365440327094,
            id: 'MARY',
          },
          target: {
            x: 653.7537879754125,
            y: 458.2462120245875,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 572.1040764008566,
            y: 228.10407640085654,
            id: 'PIANO',
          },
          target: {
            x: 653.7537879754125,
            y: 309.7537879754125,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Flip nodes',
    narration: <>Labore sint fugiat anim et id do est officia nulla.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 412,
          y: 198,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 212,
          y: 570,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 812,
          y: 198,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 612,
          y: 570,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 376.4848780721469,
            y: 264.0581267858068,
            id: 'JOHN',
          },
          target: {
            x: 247.5151219278531,
            y: 503.9418732141932,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 487,
            y: 198,
            id: 'JOHN',
          },
          target: {
            x: 737,
            y: 198,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 776.4848780721469,
            y: 264.0581267858068,
            id: 'MARY',
          },
          target: {
            x: 647.5151219278531,
            y: 503.9418732141932,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 287,
            y: 570,
            id: 'PIANO',
          },
          target: {
            x: 537,
            y: 570,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Connect bisect',
    narration: <>Eiusmod ipsum magna enim ea.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 412,
          y: 198,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 212,
          y: 570,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 812,
          y: 198,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 612,
          y: 570,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 376.4848780721469,
            y: 264.0581267858068,
            id: 'JOHN',
          },
          target: {
            x: 247.5151219278531,
            y: 503.9418732141932,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 487,
            y: 198,
            id: 'JOHN',
          },
          target: {
            x: 737,
            y: 198,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 776.4848780721469,
            y: 264.0581267858068,
            id: 'MARY',
          },
          target: {
            x: 647.5151219278531,
            y: 503.9418732141932,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 287,
            y: 570,
            id: 'PIANO',
          },
          target: {
            x: 537,
            y: 570,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 447.5151219278531,
            y: 264.0581267858068,
            id: 'JOHN',
          },
          target: {
            x: 576.4848780721469,
            y: 503.9418732141932,
            id: 'FRANCE',
          },
          text: 'VISITED',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Tetrahedral',
    narration: <>Deserunt nisi sit deserunt occaecat voluptate.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 512,
          y: 312,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 224,
          y: 168.00000000000006,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 800,
          y: 168,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 512,
          y: 600,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 444.91796067500627,
            y: 278.45898033750314,
            id: 'JOHN',
          },
          target: {
            x: 291.08203932499373,
            y: 201.5410196624969,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 579.0820393249937,
            y: 278.45898033750314,
            id: 'JOHN',
          },
          target: {
            x: 732.9179606750063,
            y: 201.54101966249684,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 758.3974852831078,
            y: 230.40377207533828,
            id: 'MARY',
          },
          target: {
            x: 553.6025147168922,
            y: 537.5962279246617,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 265.6025147168922,
            y: 230.40377207533834,
            id: 'PIANO',
          },
          target: {
            x: 470.3974852831078,
            y: 537.5962279246618,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 512,
            y: 387,
            id: 'JOHN',
          },
          target: {
            x: 512,
            y: 525,
            id: 'FRANCE',
          },
          text: 'VISITED',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 725,
            y: 168,
            id: 'MARY',
          },
          target: {
            x: 299,
            y: 168.00000000000006,
            id: 'PIANO',
          },
          text: 'OWNS',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Swap centre',
    narration: <>Et occaecat incididunt incididunt eiusmod enim dolore et.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 512,
          y: 600,
          radius: 60,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 224,
          y: 168.00000000000006,
          radius: 60,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 800,
          y: 168,
          radius: 60,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 512,
          y: 312,
          radius: 60,
        },
      ],
      links: [
        {
          source: {
            x: 470.3974852831078,
            y: 537.5962279246618,
            id: 'JOHN',
          },
          target: {
            x: 265.6025147168922,
            y: 230.40377207533828,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 553.6025147168922,
            y: 537.5962279246618,
            id: 'JOHN',
          },
          target: {
            x: 758.3974852831078,
            y: 230.40377207533828,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 732.9179606750063,
            y: 201.54101966249684,
            id: 'MARY',
          },
          target: {
            x: 579.0820393249937,
            y: 278.45898033750314,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 291.08203932499373,
            y: 201.5410196624969,
            id: 'PIANO',
          },
          target: {
            x: 444.91796067500627,
            y: 278.45898033750314,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 512,
            y: 525,
            id: 'JOHN',
          },
          target: {
            x: 512,
            y: 387,
            id: 'FRANCE',
          },
          text: 'VISITED',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 725,
            y: 168,
            id: 'MARY',
          },
          target: {
            x: 299,
            y: 168.00000000000006,
            id: 'PIANO',
          },
          text: 'OWNS',
          value: 3,
          displayText: true,
        },
      ],
      nodePaths: [],
    },
  },
  {
    name: 'Radial children',
    narration: <>Cupidatat consectetur in do esse.</>,
    data: {
      nodes: [
        {
          id: 'JOHN',
          color: 4,
          text: 'John',
          x: 512,
          y: 600,
          radius: 50,
        },
        {
          id: 'PIANO',
          color: 2,
          text: 'piano',
          x: 224,
          y: 168.00000000000006,
          radius: 50,
        },
        {
          id: 'MARY',
          color: 6,
          text: 'Mary',
          x: 800,
          y: 168,
          radius: 50,
        },
        {
          id: 'FRANCE',
          color: 7,
          text: 'France',
          x: 512,
          y: 312,
          radius: 60,
        },
        {
          id: 'JOHN-1',
          color: 4,
          text: '1',
          x: 628.3938142686286,
          y: 532.8,
          radius: 20,
        },
        {
          id: 'JOHN-2',
          color: 4,
          text: '2',
          x: 646.4,
          y: 600,
          radius: 20,
        },
        {
          id: 'JOHN-3',
          color: 4,
          text: '3',
          x: 628.3938142686286,
          y: 667.2,
          radius: 20,
        },
        {
          id: 'JOHN-4',
          color: 4,
          text: '4',
          x: 579.2,
          y: 716.3938142686286,
          radius: 20,
        },
        {
          id: 'JOHN-5',
          color: 4,
          text: '5',
          x: 512,
          y: 734.4,
          radius: 20,
        },
        {
          id: 'JOHN-6',
          color: 4,
          text: '6',
          x: 444.8,
          y: 716.3938142686286,
          radius: 20,
        },
        {
          id: 'JOHN-7',
          color: 4,
          text: '7',
          x: 395.60618573137145,
          y: 667.2,
          radius: 20,
        },
        {
          id: 'JOHN-8',
          color: 4,
          text: '8',
          x: 377.6,
          y: 600.0000000000001,
          radius: 20,
        },
        {
          id: 'JOHN-9',
          color: 4,
          text: '9',
          x: 395.6061857313714,
          y: 532.8000000000001,
          radius: 20,
        },
        {
          id: 'PIANO-1',
          color: 2,
          text: '1',
          x: 224,
          y: 302.4000000000001,
          radius: 20,
        },
        {
          id: 'PIANO-2',
          color: 2,
          text: '2',
          x: 156.8,
          y: 284.3938142686286,
          radius: 20,
        },
        {
          id: 'PIANO-3',
          color: 2,
          text: '3',
          x: 107.60618573137147,
          y: 235.2000000000001,
          radius: 20,
        },
        {
          id: 'PIANO-4',
          color: 2,
          text: '4',
          x: 89.6,
          y: 168.00000000000009,
          radius: 20,
        },
        {
          id: 'PIANO-5',
          color: 2,
          text: '5',
          x: 107.60618573137138,
          y: 100.80000000000014,
          radius: 20,
        },
        {
          id: 'PIANO-6',
          color: 2,
          text: '6',
          x: 156.79999999999995,
          y: 51.606185731371525,
          radius: 20,
        },
        {
          id: 'PIANO-7',
          color: 2,
          text: '7',
          x: 223.99999999999997,
          y: 33.60000000000005,
          radius: 20,
        },
        {
          id: 'PIANO-8',
          color: 2,
          text: '8',
          x: 291.19999999999993,
          y: 51.60618573137144,
          radius: 20,
        },
        {
          id: 'PIANO-9',
          color: 2,
          text: '9',
          x: 340.3938142686285,
          y: 100.8,
          radius: 20,
        },
        {
          id: 'MARY-1',
          color: 6,
          text: '1',
          x: 683.6061857313714,
          y: 100.79999999999998,
          radius: 20,
        },
        {
          id: 'MARY-2',
          color: 6,
          text: '2',
          x: 732.8000000000001,
          y: 51.60618573137141,
          radius: 20,
        },
        {
          id: 'MARY-3',
          color: 6,
          text: '3',
          x: 800,
          y: 33.599999999999994,
          radius: 20,
        },
        {
          id: 'MARY-4',
          color: 6,
          text: '4',
          x: 867.2,
          y: 51.606185731371454,
          radius: 20,
        },
        {
          id: 'MARY-5',
          color: 6,
          text: '5',
          x: 916.3938142686286,
          y: 100.80000000000004,
          radius: 20,
        },
        {
          id: 'MARY-6',
          color: 6,
          text: '6',
          x: 934.4,
          y: 167.99999999999997,
          radius: 20,
        },
        {
          id: 'MARY-7',
          color: 6,
          text: '7',
          x: 916.3938142686285,
          y: 235.2000000000001,
          radius: 20,
        },
        {
          id: 'MARY-8',
          color: 6,
          text: '8',
          x: 867.2000000000002,
          y: 284.3938142686285,
          radius: 20,
        },
        {
          id: 'MARY-9',
          color: 6,
          text: '9',
          x: 800,
          y: 302.4,
          radius: 20,
        },
      ],
      links: [
        {
          source: {
            x: 475.9444872453601,
            y: 545.9167308680402,
            id: 'JOHN',
          },
          target: {
            x: 260.0555127546399,
            y: 222.08326913195987,
            id: 'PIANO',
          },
          text: 'PLAYS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 548.05551275464,
            y: 545.9167308680402,
            id: 'JOHN',
          },
          target: {
            x: 763.94448724536,
            y: 222.0832691319598,
            id: 'MARY',
          },
          text: 'LIKES',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 741.8622325850055,
            y: 197.06888370749726,
            id: 'MARY',
          },
          target: {
            x: 579.0820393249937,
            y: 278.45898033750314,
            id: 'FRANCE',
          },
          text: 'LIVES_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 282.1377674149945,
            y: 197.06888370749732,
            id: 'PIANO',
          },
          target: {
            x: 444.91796067500627,
            y: 278.45898033750314,
            id: 'FRANCE',
          },
          text: 'MADE_IN',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 512,
            y: 535,
            id: 'JOHN',
          },
          target: {
            x: 512,
            y: 387,
            id: 'FRANCE',
          },
          text: 'VISITED',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 735,
            y: 168,
            id: 'MARY',
          },
          target: {
            x: 289,
            y: 168.00000000000006,
            id: 'PIANO',
          },
          text: 'OWNS',
          value: 3,
          displayText: true,
        },
        {
          source: {
            x: 598.0829251361732,
            y: 550.3,
            id: 'JOHN-1',
          },
          target: {
            x: 568.2916512459885,
            y: 567.5,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 611.4,
            y: 600,
            id: 'JOHN-2',
          },
          target: {
            x: 577,
            y: 600,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 598.0829251361732,
            y: 649.7,
            id: 'JOHN-3',
          },
          target: {
            x: 568.2916512459885,
            y: 632.5,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 561.7,
            y: 686.0829251361732,
            id: 'JOHN-4',
          },
          target: {
            x: 544.5,
            y: 656.2916512459885,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 512,
            y: 699.4,
            id: 'JOHN-5',
          },
          target: {
            x: 512,
            y: 665,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 462.3,
            y: 686.0829251361732,
            id: 'JOHN-6',
          },
          target: {
            x: 479.5,
            y: 656.2916512459885,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 425.9170748638268,
            y: 649.7,
            id: 'JOHN-7',
          },
          target: {
            x: 455.70834875401147,
            y: 632.5,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 412.6,
            y: 600.0000000000001,
            id: 'JOHN-8',
          },
          target: {
            x: 447,
            y: 600,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 425.91707486382677,
            y: 550.3000000000001,
            id: 'JOHN-9',
          },
          target: {
            x: 455.70834875401147,
            y: 567.5,
            id: 'JOHN',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 224,
            y: 267.4000000000001,
            id: 'PIANO-1',
          },
          target: {
            x: 224,
            y: 233.00000000000006,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 174.3,
            y: 254.08292513617326,
            id: 'PIANO-2',
          },
          target: {
            x: 191.5,
            y: 224.2916512459886,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 137.91707486382683,
            y: 217.7000000000001,
            id: 'PIANO-3',
          },
          target: {
            x: 167.7083487540115,
            y: 200.50000000000009,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 124.6,
            y: 168.00000000000009,
            id: 'PIANO-4',
          },
          target: {
            x: 159,
            y: 168.00000000000006,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 137.91707486382674,
            y: 118.30000000000011,
            id: 'PIANO-5',
          },
          target: {
            x: 167.70834875401147,
            y: 135.5000000000001,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 174.29999999999995,
            y: 81.91707486382687,
            id: 'PIANO-6',
          },
          target: {
            x: 191.49999999999997,
            y: 111.70834875401155,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 223.99999999999997,
            y: 68.60000000000005,
            id: 'PIANO-7',
          },
          target: {
            x: 224,
            y: 103.00000000000006,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 273.69999999999993,
            y: 81.9170748638268,
            id: 'PIANO-8',
          },
          target: {
            x: 256.49999999999994,
            y: 111.70834875401152,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 310.0829251361732,
            y: 118.30000000000001,
            id: 'PIANO-9',
          },
          target: {
            x: 280.2916512459885,
            y: 135.50000000000003,
            id: 'PIANO',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 713.9170748638268,
            y: 118.29999999999998,
            id: 'MARY-1',
          },
          target: {
            x: 743.7083487540115,
            y: 135.5,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 750.3000000000001,
            y: 81.91707486382677,
            id: 'MARY-2',
          },
          target: {
            x: 767.5,
            y: 111.70834875401147,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 800,
            y: 68.6,
            id: 'MARY-3',
          },
          target: {
            x: 800,
            y: 103,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 849.7,
            y: 81.9170748638268,
            id: 'MARY-4',
          },
          target: {
            x: 832.5,
            y: 111.7083487540115,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 886.0829251361732,
            y: 118.30000000000003,
            id: 'MARY-5',
          },
          target: {
            x: 856.2916512459885,
            y: 135.50000000000003,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 899.4,
            y: 167.99999999999997,
            id: 'MARY-6',
          },
          target: {
            x: 865,
            y: 168,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 886.0829251361731,
            y: 217.70000000000007,
            id: 'MARY-7',
          },
          target: {
            x: 856.2916512459885,
            y: 200.50000000000006,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 849.7000000000002,
            y: 254.08292513617317,
            id: 'MARY-8',
          },
          target: {
            x: 832.5000000000001,
            y: 224.29165124598848,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
        {
          source: {
            x: 800,
            y: 267.4,
            id: 'MARY-9',
          },
          target: {
            x: 800,
            y: 233,
            id: 'MARY',
          },
          text: 'RELATES_TO',
          value: 2,
          displayText: false,
        },
      ],
      nodePaths: [],
    },
  },
]

export default bakedData
