import React from 'react'
import { Theatre, Scene } from '../../../Theatre'
import { render } from './render'
import data from './derrived-data'

const TheatreViewer = () => (
  <Theatre controlBrowser>
    {data.map(s => (
      <Scene key={`scene-${s.name}`} name={s.name} narration={s.narration} data={s.data} render={render}>
        <defs>
          <marker
            id="triangle"
            viewBox="0 0 15 15"
            refX="8"
            refY="5"
            markerUnits="strokeWidth"
            markerWidth="10"
            markerHeight="10"
            orient="auto"
          >
            <path d="M 0 1 L 10 5 L 0 9 z" fill="#8F8F8F" />
          </marker>
        </defs>
      </Scene>
    ))}
  </Theatre>
)

export default TheatreViewer
