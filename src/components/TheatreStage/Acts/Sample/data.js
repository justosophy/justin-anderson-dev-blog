import React from 'react'

import pointspace from '../../pointspace'

import {
  // Data functions
  cloneObject,
  node,
  link,
  ringAboutPoint,
  addNodesToDict,
  nodePath,
  updateSceneData,
  baseScene,
} from '../../data-fns'

const point = pointspace()

const makeRing = ringAboutPoint(point)

const JohnText = (
  <code
    style={{
      backgroundColor: 'rgb(24, 91, 72)',
      color: '#ffffff',
      fontFamily: 'inherit',
      display: 'inline-block',
      padding: '0.05rem 0.75rem',
      fontSize: '0.75em',
      margin: '0 0.2em',
      lineHeight: '1.8rem',
    }}
  >
    John
  </code>
)

const PianoText = (
  <code
    style={{
      backgroundColor: 'rgb(204, 165, 227)',
      color: 'rgba(0, 0, 0, 0.9)',
      fontFamily: 'inherit',
      display: 'inline-block',
      padding: '0.05rem 0.75rem',
      fontSize: '0.75em',
      margin: '0 0.2em',
      lineHeight: '1.8rem',
    }}
  >
    Piano
  </code>
)

const MaryText = (
  <code
    style={{
      backgroundColor: 'rgb(24, 91, 72)',
      color: '#ffffff',
      fontFamily: 'inherit',
      display: 'inline-block',
      padding: '0.05rem 0.75rem',
      fontSize: '0.75em',
      margin: '0 0.2em',
      lineHeight: '1.8rem',
    }}
  >
    Mary
  </code>
)

const beginScene = cloneObject(baseScene)
beginScene.name = 'Begin'
beginScene.narration = data => (
  <>
    <p>
      This narrative is told through a diagram in the form of a{' '}
      <a href="https://en.wikipedia.org/wiki/Directed_graph" target="_blank" rel="noopener noreferrer">
        directed graph.
      </a>
    </p>
    <p>
      Prose is used to annotate but our story is formed by <em>relationship</em>. These are textual and spatial.
    </p>
    <p>Our narrative is about a character called {JohnText}.</p>
    <p>
      <em>"John plays piano."</em>
    </p>
    <p>
      The centered position, the dominant color, the direction of the arrow - these inform how to turn the diagram into
      speech.
    </p>
    <p>We update the visual and its annotations to continue the story.</p>
    <p>
      <em>
        ~ Select the{' '}
        <button
          type="button"
          onClick={() => {
            if (data) {
              data.nextScene()
            }
          }}
          style={{ display: 'inline', WebkitAppearance: 'none', appearance: 'none' }}
        >
          next scene
        </button>
        .
      </em>
    </p>
  </>
)
beginScene.narration.displayName = 'Narration'

updateSceneData(beginScene, ({ nodes, links }) => {
  nodes.john = point(node('John', { color: 3 }))
    .center()
    .moveLeft()
    .value()

  nodes.piano = point(node('piano', { color: 8 }))
    .center()
    .moveUp(2)
    .moveRight(2)
    .value()

  links.push(link(nodes.john, 'PLAYS', nodes.piano))
})

const conjunction = cloneObject(beginScene)
conjunction.name = 'Conjunction'
conjunction.narration = (
  <>
    <p>
      <em>"John plays piano and also likes Mary."</em>
    </p>
    <p>We introduce a relationship to {MaryText}. Color unifies her with John, but his story remains the focus.</p>
    <p>Mary takes the plot in a new direction both thematically and spatially.</p>
    <p>
      The {PianoText} playing moves over with the introduction of Mary. There is a divergence from John when compared
      with the last scene.
    </p>
    <p>Perhaps we should change the reading to recognise this bifurcation,</p>
    <p>
      <em>
        "John plays piano <strong>but also</strong> likes Mary."
      </em>
    </p>
    <p>Now the diagram communicates temporally as well as spatially.</p>
    <p>
      <em>~ Use the 'right arrow' key or 'next' button to select the next scene.</em>
    </p>
  </>
)
updateSceneData(conjunction, ({ nodes, links }) => {
  nodes.piano = point(nodes.piano)
    .moveLeft(1)
    .moveUp(1)
    .value()

  nodes.mary = point(node('Mary', { color: 3 }))
    .center()
    .moveDown(3)
    .moveRight(1)
    .value()

  links.push(link(nodes.john, 'LIKES', nodes.mary))
})

const convergence = cloneObject(conjunction)
convergence.name = 'Convergence'
convergence.narration = <>Irure id eiusmod exercitation ipsum dolore commodo duis.</>
updateSceneData(convergence, ({ nodes, links }) => {
  nodes.john = point(nodes.john)
    .moveLeft(2)
    .value()

  nodes.piano = point(nodes.piano)
    .centerX()
    .value()

  nodes.mary = point(nodes.mary)
    .centerX()
    .value()

  nodes.france = point(node('France', { color: 5 }))
    .center()
    .moveRight(3)
    .value()

  links.push(link(nodes.mary, 'LIVES_IN', nodes.france))
  links.push(link(nodes.piano, 'MADE_IN', nodes.france))
})

const tracePath = cloneObject(convergence)
tracePath.name = 'Trace path'
tracePath.narration = <>Magna dolor cupidatat culpa anim Lorem elit culpa ex occaecat.</>
updateSceneData(tracePath, ({ nodes, nodePaths }) => {
  nodePaths.nodePath1 = nodePath('nodePath1', [nodes.mary, nodes.john, nodes.piano], { color: 2 })
})

const overlapPaths = cloneObject(tracePath)
overlapPaths.name = 'Overlap paths'
overlapPaths.narration = <>Magna dolor cupidatat culpa anim Lorem elit culpa ex occaecat.</>
updateSceneData(overlapPaths, ({ nodes, nodePaths }) => {
  nodePaths.nodePath2 = nodePath('nodePath2', [nodes.france, nodes.piano, nodes.mary], {
    color: 6,
  })
})

const adjustNodes = cloneObject(overlapPaths)
adjustNodes.name = 'Adjust nodes'
adjustNodes.narration = <>Tempor excepteur tempor amet mollit.</>
updateSceneData(adjustNodes, ({ nodes, nodePaths }) => {
  nodes.john.color = 4
  nodes.john.radius = 25

  nodes.piano.color = 2
  nodes.piano.radius = 70

  nodes.mary.color = 6
  nodes.mary.radius = 8

  nodes.france.color = 7
  nodes.france.radius = 90

  delete nodePaths.nodePath1
  delete nodePaths.nodePath2
})

const flipNodes = cloneObject(adjustNodes)
flipNodes.name = 'Flip nodes'
flipNodes.narration = <>Labore sint fugiat anim et id do est officia nulla.</>
updateSceneData(flipNodes, ({ nodes }) => {
  nodes.john = point(nodes.john)
    .update({ radius: 60 })
    .ofWidth(2 / 5)
    .ofHeight(3 / 12)
    .value()

  nodes.piano = point(nodes.piano)
    .update({ radius: 60 })
    .ofWidth(1 / 5)
    .ofHeight(9 / 12)
    .value()

  nodes.mary = point(nodes.mary)
    .update({ radius: 60 })
    .ofWidth(4 / 5)
    .ofHeight(3 / 12)
    .value()

  nodes.france = point(nodes.france)
    .update({ radius: 60 })
    .ofWidth(3 / 5)
    .ofHeight(9 / 12)
    .value()
})

const connectBisect = cloneObject(flipNodes)
connectBisect.name = 'Connect bisect'
connectBisect.narration = <>Eiusmod ipsum magna enim ea.</>
updateSceneData(connectBisect, ({ nodes, links }) => {
  links.push(link(nodes.john, 'VISITED', nodes.france))
})

const tetrahedral = cloneObject(connectBisect)
tetrahedral.name = 'Tetrahedral'
tetrahedral.narration = <>Deserunt nisi sit deserunt occaecat voluptate.</>
updateSceneData(tetrahedral, ({ nodes, links }) => {
  nodes.john = point(nodes.john)
    .center()
    .moveUp()
    .value()

  nodes.piano = point(nodes.piano)
    .rotateAboutPoint(nodes.john, 6 / 12, 4)
    .moveUp(2)
    .value()

  nodes.mary = point(nodes.mary)
    .rotateAboutPoint(nodes.john, 0, 4)
    .moveUp(2)
    .value()

  nodes.france = point(nodes.france)
    .rotateAboutPoint(nodes.john, 3 / 12, 4)
    .value()

  links.push(link(nodes.mary, 'OWNS', nodes.piano))
})

const swapCentre = cloneObject(tetrahedral)
swapCentre.name = 'Swap centre'
swapCentre.narration = <>Et occaecat incididunt incididunt eiusmod enim dolore et.</>
updateSceneData(swapCentre, ({ nodes }) => {
  nodes.john = point(nodes.john)
    .rotateAboutPoint(nodes.john, 3 / 12, 4)
    .value()

  nodes.france = point(nodes.france)
    .center()
    .moveUp()
    .value()
})

const radialChildren = cloneObject(swapCentre)
radialChildren.name = 'Radial children'
radialChildren.narration = <>Cupidatat consectetur in do esse.</>
updateSceneData(radialChildren, ({ nodes, links }) => {
  nodes.john.radius = 50
  const johnRing = makeRing(nodes.john, { number: 9, turn: 1 / 12, start: -1 / 12 })
  addNodesToDict(nodes, johnRing)
  links.push(...johnRing.map(v => link(v, 'RELATES_TO', nodes.john, { displayText: false, value: 2 })))

  nodes.piano.radius = 50
  const pianoRing = makeRing(nodes.piano, { number: 9, turn: 1 / 12, start: 3 / 12 })
  addNodesToDict(nodes, pianoRing)
  links.push(...pianoRing.map(v => link(v, 'RELATES_TO', nodes.piano, { displayText: false, value: 2 })))

  nodes.mary.radius = 50
  const maryRing = makeRing(nodes.mary, { number: 9, turn: 1 / 12, start: 7 / 12 })
  addNodesToDict(nodes, maryRing)
  links.push(...maryRing.map(v => link(v, 'RELATES_TO', nodes.mary, { displayText: false, value: 2 })))
})

export default [
  beginScene,
  conjunction,
  convergence,
  tracePath,
  overlapPaths,
  adjustNodes,
  flipNodes,
  connectBisect,
  tetrahedral,
  swapCentre,
  radialChildren,
]
