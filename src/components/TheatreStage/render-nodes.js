import { interpolateCubehelixDefault } from 'd3-scale-chromatic'
import { updateAndEnterSelections } from './d3-fns'

const dotProduct = (a, b) => {
  if (a.length !== b.length) throw new Error("Array lengths don't match")
  let d = 0
  for (let i = 0, len = a.length; i < len; i += 1) {
    d += a[i] * b[i]
  }
  return d
}

const samples = [['M', 16.249906539916992], ['n', 11.047125816345215], ['.', 4.789721488952637]]
const distribution = [0.06, 0.8, 0.14]
const avgText = dotProduct(samples.map(v => v[1]), distribution)
const textWidthEstimator = str => str.length * avgText

const RADIAL_TEXT_BOUND = 1.6

const textFits = d => {
  const textWidth = textWidthEstimator(d.text.toString())
  const radialLimit = d.radius * RADIAL_TEXT_BOUND
  return textWidth < radialLimit
}

const textDy = d => {
  const fit = textFits(d)
  if (fit) {
    return 7
  }
  return d.radius + 30
}

const textFill = d => {
  const fit = textFits(d)
  if (!fit) {
    return '#333'
  }
  if (d.color > 6) {
    return 'rgba(0,0,0,0.9)'
  }
  return 'rgba(255,255,255,0.9)'
}

const outerNodeFill = d => {
  const fit = textFits(d)
  if (fit) {
    return 'transparent'
  }
  return interpolateCubehelixDefault(d.color * 0.09)
}

const NEAR_ZERO = 1e-6

export default (svgDoc, data) => {
  const nodeGroupUpdates = svgDoc.selectAll('.node-group').data(data.nodes, d => `node-${d.id}`)

  nodeGroupUpdates
    .exit()
    .transition()
    .attr('opacity', NEAR_ZERO)
    .remove()

  nodeGroupUpdates
    .transition()
    .attr('transform', d => `translate(${d.x},${d.y})`)
    .attr('opacity', 1)

  const enter = nodeGroupUpdates
    .enter()
    .append('g')
    .attr('id', d => `nodegroup_(${d.id})`)
    .attr('class', 'node-group')
    .attr('transform', d => `translate(${d.x},${d.y})`)
    .attr('opacity', NEAR_ZERO)

  enter.transition().attr('opacity', 1)

  const [nodeOuterEnter, nodeOuterUpdate] = updateAndEnterSelections(nodeGroupUpdates, enter, 'node-outer', 'circle')

  nodeOuterUpdate
    .transition()
    .attr('stroke', d => interpolateCubehelixDefault(d.color * 0.09))
    .attr('r', d => d.radius + 4)
    .attr('fill', outerNodeFill)

  nodeOuterEnter
    .attr('stroke-width', 3)
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('r', d => d.radius + 4)
    .attr('stroke', d => interpolateCubehelixDefault(d.color * 0.09))
    .attr('fill', outerNodeFill)

  const [nodeInnerEnter, nodeInnerUpdate] = updateAndEnterSelections(nodeGroupUpdates, enter, 'node-inner', 'circle')

  nodeInnerUpdate
    .transition()
    .attr('r', d => d.radius)
    .attr('fill', d => interpolateCubehelixDefault(d.color * 0.09))

  nodeInnerEnter
    .attr('r', d => d.radius)
    .attr('stroke-width', 0)
    .attr('fill', d => interpolateCubehelixDefault(d.color * 0.09))
    .attr('cx', 0)
    .attr('cy', 0)

  const [nodeTextEnter, nodeTextUpdate] = updateAndEnterSelections(nodeGroupUpdates, enter, 'node-text', 'text')

  nodeTextUpdate
    .text(d => d.text)
    .transition()
    .attr('dy', textDy)
    .attr('fill', textFill)

  nodeTextEnter
    .attr('text-anchor', 'middle')
    .attr('x', 0)
    .attr('y', 0)
    .attr('font-size', '20')
    .text(d => d.text)
    .attr('dy', textDy)
    .attr('fill', textFill)

  nodeGroupUpdates.merge(enter)
}
