const gradient = (p1, p2) => (p1.y - p2.y) / (p1.x - p2.x)

const midpoint = (p1, p2) => ({ x: p1.x - p2.x, y: p1.y - p2.y })

const lerp = (value1, value2, percent) => {
  let amount = percent < 0 ? 0 : percent
  amount = amount > 1 ? 1 : amount
  return value1 + (value2 - value1) * amount
}

const distance = (p1, p2) => {
  const a = p1.x - p2.x
  const b = p1.y - p2.y
  return Math.sqrt(a * a + b * b)
}

const lerpPoints = (p1, p2, amount) => ({ x: lerp(p1.x, p2.x, amount), y: lerp(p1.y, p2.y, amount) })

const offsetEndsByRadius = (p1, p2, spacing = 0) => {
  const dist = distance(p1, p2)

  const point1LerpAmount = (p1.radius + spacing) / dist
  const point1 = lerpPoints(p1, p2, point1LerpAmount)

  const point2LerpAmount = 1 - (p2.radius + spacing) / dist
  const point2 = lerpPoints(p1, p2, point2LerpAmount)

  return [point1, point2]
}

export { gradient, midpoint, distance, lerpPoints, offsetEndsByRadius }
