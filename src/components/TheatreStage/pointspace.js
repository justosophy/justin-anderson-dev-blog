const center = (MID_X, MID_Y) => p => Object.assign({}, p, { x: MID_X, y: MID_Y })

const centerX = MID_X => p => Object.assign({}, p, { x: MID_X })

const centerY = MID_Y => p => Object.assign({}, p, { y: MID_Y })

const leftEdge = border => p => {
  let addRadius = 0
  if (p.radius) {
    addRadius = p.radius
  }
  return Object.assign({}, p, { x: border + addRadius })
}

const rightEdge = (width, border) => p => {
  let addRadius = 0
  if (p.radius) {
    addRadius = p.radius
  }
  return Object.assign({}, p, { x: width - (border + addRadius) })
}

const topEdge = border => p => {
  let addRadius = 0
  if (p.radius) {
    addRadius = p.radius
  }
  return Object.assign({}, p, { y: border + addRadius })
}

const bottomEdge = (height, border) => p => {
  let addRadius = 0
  if (p.radius) {
    addRadius = p.radius
  }
  return Object.assign({}, p, { y: height - (border + addRadius) })
}

const moveLeft = border => (p, n = 1) => {
  let moveSize = border
  if (p.radius) {
    moveSize = border + p.radius
  }
  moveSize *= n
  return Object.assign({}, p, { x: p.x - moveSize })
}

const moveRight = border => (p, n = 1) => {
  let moveSize = border
  if (p.radius) {
    moveSize = border + p.radius
  }
  moveSize *= n
  return Object.assign({}, p, { x: p.x + moveSize })
}

const moveUp = border => (p, n = 1) => {
  let moveSize = border
  if (p.radius) {
    moveSize = border + p.radius
  }
  moveSize *= n
  return Object.assign({}, p, { y: p.y - moveSize })
}

const moveDown = border => (p, n = 1) => {
  let moveSize = border
  if (p.radius) {
    moveSize = border + p.radius
  }
  moveSize *= n
  return Object.assign({}, p, { y: p.y + moveSize })
}

const ofWidth = (width, border) => (p, amount = 0) => {
  const boundary = width - border * 2
  return Object.assign({}, p, { x: border + boundary * amount })
}

const ofHeight = (height, border) => (p, amount = 0) => {
  const boundary = height - border * 2
  return Object.assign({}, p, { y: border + boundary * amount })
}

const rotateAboutPoint = border => (p, point, turnAmount, amount = 1) => {
  let moveSize = border
  if (p.radius) {
    moveSize = border + p.radius
  }
  moveSize *= amount

  const ONE_TURN = Math.PI * 2

  const dx = moveSize * Math.cos(turnAmount * ONE_TURN)
  const dy = moveSize * Math.sin(turnAmount * ONE_TURN)
  return Object.assign({}, p, { x: point.x + dx, y: point.y + dy })
}

const update = (p, props = {}) => Object.assign({}, p, props)

const pointspace = (opt = {}) => p => {
  const { width = 1024, height = 768, border = 12 } = opt

  let point = p

  const MID_X = width * 0.5
  const MID_Y = height * 0.5

  const obj = {
    value: () => point,
  }

  const methods = [
    { name: 'bottomEdge', f: bottomEdge(height, border) },
    { name: 'center', f: center(MID_X, MID_Y) },
    { name: 'centerX', f: centerX(MID_X) },
    { name: 'centerY', f: centerY(MID_Y) },
    { name: 'leftEdge', f: leftEdge(border) },
    { name: 'moveDown', f: moveDown(border) },
    { name: 'moveLeft', f: moveLeft(border) },
    { name: 'moveRight', f: moveRight(border) },
    { name: 'moveUp', f: moveUp(border) },
    { name: 'ofHeight', f: ofHeight(height, border) },
    { name: 'ofWidth', f: ofWidth(width, border) },
    { name: 'rightEdge', f: rightEdge(width, border) },
    { name: 'rotateAboutPoint', f: rotateAboutPoint(border) },
    { name: 'topEdge', f: topEdge(border) },
    { name: 'update', f: update },
  ]
  methods.forEach(m => {
    obj[m.name] = (...ar) => {
      point = m.f.apply(undefined, [point, ...ar])
      return obj
    }
  })

  return obj
}

export default pointspace
export {
  bottomEdge,
  center,
  centerX,
  centerY,
  leftEdge,
  moveDown,
  moveLeft,
  moveRight,
  moveUp,
  ofHeight,
  ofWidth,
  rightEdge,
  rotateAboutPoint,
  topEdge,
  update,
}
