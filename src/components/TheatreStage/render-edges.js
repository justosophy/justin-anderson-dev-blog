import { updateAndEnterSelections } from './d3-fns'

const key = l => `(${l.source.id})[${l.text}](${l.target.id})`

const isReversed = l => l.source.x > l.target.x

export default (svgDoc, data) => {
  const edgeGroupUpdates = svgDoc.selectAll('.edge-group').data(data.links, l => `edge-${key(l)}`)

  edgeGroupUpdates
    .exit()
    .transition()
    .style('opacity', 0)
    .remove()

  const enter = edgeGroupUpdates
    .enter()
    .append('g')
    .attr('id', l => `edgegroup_${key(l)}`)
    .attr('class', 'edge-group')
    .attr('opacity', 0)

  enter.transition().attr('opacity', 1)

  const [edgePathEnter, edgePathUpdate] = updateAndEnterSelections(edgeGroupUpdates, enter, 'edge-path', 'path')

  edgePathUpdate.transition().attr('d', l => `M${l.source.x},${l.source.y} L${l.target.x},${l.target.y}`)

  edgePathEnter
    .attr('id', l => `edge_path_${key(l)}`)
    .attr('fill', 'none')
    .attr('stroke', '#8F8F8F')
    .attr('stroke-width', l => l.value)
    .attr('marker-end', 'url(#triangle)')
    .attr('d', l => `M${l.source.x},${l.source.y} L${l.source.x},${l.source.y}`)
    .transition()
    .attr('d', l => `M${l.source.x},${l.source.y} L${l.target.x},${l.target.y}`)

  const [edgePathReversedEnter, edgePathReversedUpdate] = updateAndEnterSelections(
    edgeGroupUpdates,
    enter,
    'edge-path-reversed',
    'path'
  )

  edgePathReversedUpdate.transition().attr('d', l => `M${l.target.x},${l.target.y} L${l.source.x},${l.source.y}`)

  edgePathReversedEnter
    .attr('id', l => `edge_path_reversed_${key(l)}`)
    .attr('fill', 'none')
    .attr('stroke', 'red')
    .attr('stroke-width', 0)
    .attr('d', l => `M${l.target.x},${l.target.y} L${l.target.x},${l.target.y}`)
    .transition()
    .attr('d', l => `M${l.target.x},${l.target.y} L${l.source.x},${l.source.y}`)

  const [edgeTextEnter, edgeTextUpdate] = updateAndEnterSelections(edgeGroupUpdates, enter, 'edge-text', 'text')

  window.a = edgeTextEnter

  edgeTextUpdate.transition().style('font-size', d => `${(d.value / 3) * 0.85}em`)

  edgeTextEnter
    .attr('fill', '#555')
    .attr('dy', '-0.6em')
    .style('letter-spacing', '1px')
    .style('font-size', d => `${(d.value / 3) * 0.85}em`)

  const [edgeTextPathEnter, edgeTextPathUpdate] = updateAndEnterSelections(
    edgeTextUpdate,
    edgeTextEnter,
    'edge-text-path',
    'textPath'
  )

  edgeTextPathUpdate.attr('href', l => `#edge_path${isReversed(l) ? '_reversed' : ''}_${key(l)}`)

  edgeTextPathEnter
    .style('display', d => (d.displayText ? 'inline' : 'none'))
    .attr('startOffset', '50%')
    .attr('lengthAdjust', 'spacingAndGlyphs')
    .attr('text-anchor', 'middle')
    .attr('href', l => `#edge_path${isReversed(l) ? '_reversed' : ''}_${key(l)}`)
    .text(d => d.text)

  edgeTextUpdate.merge(edgeTextEnter)

  edgeGroupUpdates.merge(enter)
}
