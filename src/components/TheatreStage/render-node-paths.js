import { interpolateCubehelixDefault } from 'd3-scale-chromatic'

const NEAR_ZERO = 1e-6

const nodePathId = np => `nodepath-${np.nodes.map(n => n.id).join('-')}`

const STROKE_WIDTH = 16
// const STROKE_COLOR = 'rgb(143, 143, 143)'

const calculateEdgeDPath = d =>
  d.edges.map(ep => `M ${ep.source.x} ${ep.source.y} L ${ep.target.x} ${ep.target.y}`).join(' ')

export default (svgDoc, data) => {
  const nodePathGroupUpdates = svgDoc.selectAll('.node-path').data(data.nodePaths, d => nodePathId(d))

  nodePathGroupUpdates
    .exit()
    .transition()
    .attr('opacity', NEAR_ZERO)
    .remove()

  nodePathGroupUpdates.transition().attr('opacity', 1)

  const enter = nodePathGroupUpdates
    .enter()
    .append('g')
    .attr('id', d => `nodepath_(${d.id})`)
    .attr('class', 'node-path')
    .attr('opacity', NEAR_ZERO)
    .attr('stroke', d => interpolateCubehelixDefault(0.7 + d.color * 0.09 * 0.3))

  enter.transition().attr('opacity', 1)

  nodePathGroupUpdates
    .selectAll('.node-path-node')
    .data(d => d.nodes, d => d.id)
    .transition()
    .attr('cx', d => d.x)
    .attr('cy', d => d.y)
    .attr('r', d => d.radius + STROKE_WIDTH)

  enter
    .selectAll('.node-path-node')
    .data(d => d.nodes, d => d.id)
    .enter()
    .append('circle')
    .attr('class', 'node-path-node')
    .attr('data-nodeid', d => d.id)
    .attr('opacity', '0.7')
    .attr('fill', 'transparent')
    .attr('stroke-width', STROKE_WIDTH)
    .style('pointer-events', 'none')
    .attr('cx', d => d.x)
    .attr('cy', d => d.y)
    .attr('r', d => d.radius + STROKE_WIDTH)

  nodePathGroupUpdates
    .selectAll('.node-path-edge')
    .data(d => [{ edges: d.edges, id: d.id }], d => d.id)
    .transition()
    .attr('d', calculateEdgeDPath)

  enter
    .selectAll('.node-path-edge')
    .data(d => [{ edges: d.edges, id: d.id }], d => d.id)
    .enter()
    .append('path')
    .attr('class', 'node-path-edge')
    .attr('opacity', '0.7')
    .attr('stroke-width', '24')
    .attr('fill', 'none')
    .style('pointer-events', 'none')
    .attr('d', calculateEdgeDPath)

  nodePathGroupUpdates.merge(enter)
}
