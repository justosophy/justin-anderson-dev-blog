import React from 'react'

export const cloneObject = obj => JSON.parse(JSON.stringify(obj))

export const node = (text, props = {}) => {
  const { id = text.toUpperCase().replace(/ /g, '_'), color = 5, x = 0, y = 0, radius = 60 } = props

  const obj = {
    id,
    color,
    text,
    x,
    y,
    radius,
  }
  return obj
}

export const link = (source, text, target, props = {}) => {
  const { value = 3, displayText = true } = props
  return {
    source: source.id,
    target: target.id,
    text,
    value,
    displayText,
  }
}

export const ringAboutPoint = pointFn => (p, opts = {}) => {
  const { number = 12 } = opts
  const { turn = 1 / number, start = 0 } = opts
  return new Array(number).fill(p).map((v, i) =>
    pointFn({ ...v, radius: 20, id: `${v.id}-${i + 1}`, text: (i + 1).toString() })
      .rotateAboutPoint(v, i * turn + start, 4.2)
      .value()
  )
}

export const addNodesToDict = (nodeDictionary, nodesToAdd) => {
  nodesToAdd.forEach(v => {
    nodeDictionary[v.id] = v
  })
}

export const nodePath = (id, nodes, props = {}) => {
  const { color = 5 } = props
  const obj = {
    id,
    nodes: nodes.map(n => n.id),
    color,
  }
  return obj
}

export const updateSceneData = (scene, fn) => fn(scene.data)

export const baseScene = {
  // Base scene object
  name: 'Scene Name',
  narration: <></>,
  data: { nodes: {}, links: [], nodePaths: {} },
}
