import styled from 'styled-components'

export const Tablet = styled.div`
  display: none;
  @media only (max-width: ${props => props.theme.breakpoints.tablet}) {
    background: red;
  }
`
export const Phone = styled.div`
  display: none;
  @media (max-width: ${props => props.theme.breakpoints.phone}) ) {
    display: block;
  }
`

export const PhonePortrait = styled.div`
  display: none;
  @media (max-width: ${props => props.theme.breakpoints.phone}) and (orientation: portrait) {
    display: block;
  }
`

export const PhoneLandscape = styled.div`
  display: none;
  @media (max-width: ${props => props.theme.breakpoints.phone}) and (orientation: landscape) {
    display: block;
  }
`
