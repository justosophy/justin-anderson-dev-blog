import React from 'react'

const SocialLinks = () => (
  <>
    <a href="https://twitter.com/justosophy" target="_blank" rel="noopener noreferrer">
      Twitter
    </a>
    ,{' '}
    <a href="https://www.linkedin.com/in/justinivananderson/" target="_blank" rel="noopener noreferrer">
      LinkedIn
    </a>
  </>
)

export default SocialLinks
