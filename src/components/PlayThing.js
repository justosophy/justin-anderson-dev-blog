import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  pre {
    display: inline-block;
    user-select: none;
    opacity: 0.5;
  }
`

// const eye = pos => {
//   switch (pos) {
//     default:
//       return '-'
//   }
// }

const thing = () => `
   __
  /||\\
(-)  (-)
 \\____/
  \\||/
  /||\\
`

const PlayThing = () => (
  <Wrapper>
    <pre>{thing()}</pre>
  </Wrapper>
)

export default PlayThing
