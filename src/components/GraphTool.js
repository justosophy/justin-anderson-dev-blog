import React from 'react'
import styled from 'styled-components'
import Button from './Button'

const Wrapper = styled.div`
  svg {
    display: block;
    max-width: 100% !important;
    height: auto !important;
    margin: 2em auto;
    border: solid 2px #eee;
  }
`

const GraphTool = () => (
  <Wrapper>
    <div>
      <svg viewBox="0 0 1000 600" xmlns="http://www.w3.org/2000/svg">
        <g transform="translate(100, 100)">
          <circle r={50} cx={0} cy={0} fill="#aaaaaa" stroke="#888888" strokeWidth={2} />
          <text textAnchor="middle" transform="translate(0,6)">
            Person
          </text>
        </g>
        <g transform="translate(400, 200)">
          <circle r={50} cx={0} cy={0} fill="#aaaaaa" stroke="#888888" strokeWidth={2} />
          <text textAnchor="middle" transform="translate(0,6)">
            Chocolate
          </text>
        </g>
      </svg>
      <Button big>Graph Tool</Button>
    </div>
  </Wrapper>
)

export default GraphTool
