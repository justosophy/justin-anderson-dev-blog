/* eslint-disable react/prop-types */

import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'

import Button from '../Button'

const WIDTH = 1024
const HEIGHT = 768

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 2rem calc(50% - 50vw) 0rem;
  overflow: hidden;
  .theatre-aside {
    position: relative;
    padding: 3.6rem 1.4rem 2rem;
    background: #fff;
  }
  svg {
    display: block;
    background-color: #f9f9f9;
    border-top: solid 2px #eee;
    border-bottom: solid 2px #eee;
  }

  flex-direction: column-reverse;

  .theatre-controls {
    position: absolute;
    top: 1rem;
    right: 1.4rem;
  }

  .theatre-description-title {
    margin: 0 0 1rem;
    padding-top: 0.2rem;
  }

  @media (min-width: ${props => props.theme.breakpoints.phone}) and (orientation: landscape) {
    flex-direction: row;
    height: 600px;
    .theatre-aside {
      width: 323px;
      box-shadow: 1px 0 6px #eee;
      border-top: solid 2px #eee;
      border-bottom: solid 2px #eee;
      border-left: solid 2px #eee;
      border-right: solid 1px #eee;
    }
    .theatre-aside:before {
      content: ' ';
      pointer-events: none;
      position: absolute;
      top: 3.5rem;
      left: 0;
      right: 0;
      height: 0.8rem;
      background: linear-gradient(rgba(255, 255, 255, 1), rgba(255, 255, 255, 0));
    }
    .theatre-aside:after {
      content: ' ';
      pointer-events: none;
      position: absolute;
      bottom: 2rem;
      left: 0;
      right: 0;
      height: 2rem;
      background: linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 1));
    }
    .theatre-description {
      overflow-y: scroll;
      height: calc(100% - 1rem);
      padding-bottom: 1rem;
    }
    svg {
      flex: 1;
      display: block;
      background-color: #f9f9f9;
      max-width: 800px;
      border-right: solid 2px #eee;
    }
  }

  @media (min-width: ${props => props.theme.breakpoints.tablet}) {
    .theatre-aside {
      border-top-left-radius: 0.5rem;
      border-bottom-left-radius: 0.5rem;
    }
    svg {
      border-top-right-radius: 0.5rem;
      border-bottom-right-radius: 0.5rem;
    }
  }
`

const Scene = props => {
  const { children } = props
  return <>{children}</>
}

const Theatre = props => {
  const { startScene = 0, controlBrowser = false, children } = props

  let initialSceneIndex = -1

  const scenes = React.Children.toArray(children)

  if (Number.isInteger(startScene) && startScene >= 0 && startScene < scenes.length) {
    initialSceneIndex = startScene
  } else if (typeof startScene === 'string') {
    initialSceneIndex = children.findIndex(s => s.name === startScene)
  }

  if (initialSceneIndex === -1) {
    return <pre>no scene found</pre>
  }

  let svgDom = useRef(null)
  let narrationDom = useRef(null)

  const pathRef = useRef('')
  if (typeof window !== 'undefined') {
    pathRef.current = window.location.pathname
  }

  const [sceneIndex, updateSceneIndex] = useState(initialSceneIndex)

  const scene = scenes[sceneIndex].props

  useEffect(() => {
    scene.render(svgDom, scene.data)
    if (narrationDom && narrationDom.scrollIntoViewIfNeeded) {
      narrationDom.scrollIntoViewIfNeeded()
    }
  }, [sceneIndex])

  useEffect(() => {
    if (typeof window === 'undefined') {
      return
    }

    if (!controlBrowser) {
      return
    }

    const popstateListener = event => {
      if (pathRef.current !== window.location.pathname) {
        return
      }

      let si = 0
      if (event.state !== null && typeof event.state === 'object' && Number.isInteger(event.state.sceneIndex)) {
        si = event.state.sceneIndex
      }
      window.history.replaceState({ sceneIndex: si }, '', `#scene-${si + 1}`)
      updateSceneIndex(si)
    }

    window.addEventListener('popstate', popstateListener)

    const hashCheck = () => {
      /* eslint-disable prefer-destructuring */
      const hash = window.location.hash
      /* eslint-enable prefer-destructuring */

      let historySceneIndex = sceneIndex

      if (/^#scene-(\d+)$/.test(hash)) {
        const sceneNum = parseInt(hash.replace(/.*(\d+).*/, '$1'), 10)
        if (Number.isInteger(sceneNum) && sceneNum > 0 && sceneNum <= scenes.length) {
          historySceneIndex = sceneNum - 1
        }
      }

      window.history.replaceState({ sceneIndex: historySceneIndex }, '', `#scene-${historySceneIndex + 1}`)
      if (historySceneIndex > 0) {
        updateSceneIndex(historySceneIndex)
      }
    }

    const keyUpListener = event => {
      if (typeof window.history.state !== 'object' || !Number.isInteger(window.history.state.sceneIndex)) {
        return
      }

      if (event.keyCode === 37) {
        // Arrow left
        const prevSceneIndex = window.history.state.sceneIndex - 1
        if (prevSceneIndex >= 0) {
          window.history.pushState({ sceneIndex: prevSceneIndex }, '', `#scene-${prevSceneIndex + 1}`)
          updateSceneIndex(prevSceneIndex)
        }
      } else if (event.keyCode === 39) {
        // Arrow up
        const nextSceneIndex = window.history.state.sceneIndex + 1
        if (nextSceneIndex < scenes.length) {
          window.history.pushState({ sceneIndex: nextSceneIndex }, '', `#scene-${nextSceneIndex + 1}`)
          updateSceneIndex(nextSceneIndex)
        }
      }
    }

    window.addEventListener('keyup', keyUpListener)

    hashCheck()

    return () => {
      window.removeEventListener('popstate', popstateListener)
      window.removeEventListener('keyup', keyUpListener)
    }
  }, [])

  const narrationProps = {
    scene,
    nextScene: () => {
      window.history.pushState({ sceneIndex: sceneIndex + 1 }, '', `#scene-${sceneIndex + 2}`)
      updateSceneIndex(sceneIndex + 1)
    },
  }

  return (
    <Wrapper>
      <div className="theatre-aside">
        <div className="theatre-controls">
          <Button
            onClick={() => {
              window.history.pushState({ sceneIndex: sceneIndex - 1 }, '', `#scene-${sceneIndex}`)
              updateSceneIndex(sceneIndex - 1)
            }}
            disabled={sceneIndex === 0}
            style={{ marginRight: '0.5rem' }}
          >
            prev
          </Button>
          <Button
            onClick={() => {
              window.history.pushState({ sceneIndex: sceneIndex + 1 }, '', `#scene-${sceneIndex + 2}`)
              updateSceneIndex(sceneIndex + 1)
            }}
            disabled={sceneIndex === scenes.length - 1}
          >
            next
          </Button>
        </div>
        <div className="theatre-description" aria-live="polite" aria-atomic="true">
          <h3
            ref={el => {
              narrationDom = el
            }}
            className="theatre-description-title"
          >
            {scene.name}
          </h3>
          <div>{typeof scene.narration === 'function' ? scene.narration(narrationProps) : scene.narration}</div>
        </div>
      </div>
      <svg
        viewBox={`0 0 ${WIDTH} ${HEIGHT}`}
        xmlns="http://www.w3.org/2000/svg"
        ref={el => {
          svgDom = el
        }}
      >
        {scene.children}
      </svg>
    </Wrapper>
  )
}

export { Theatre, Scene }
