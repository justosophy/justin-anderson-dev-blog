import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import styled from 'styled-components'

import { Layout, Wrapper, Header, SectionTitle, PlayThing } from '../components'
import config from '../../config'

const Content = styled.div`
  grid-column: 2;
  box-shadow: 0 4px 120px rgba(0, 0, 0, 0.1);
  border-radius: 1rem;
  padding: 2rem 4rem;
  background-color: ${props => props.theme.colors.bg};
  z-index: 9000;
  margin-top: -3rem;
  @media (max-width: ${props => props.theme.breakpoints.tablet}) {
    padding: 3rem 3rem;
  }
  @media (max-width: ${props => props.theme.breakpoints.phone}) {
    padding: 2rem 1.5rem;
    border-radius: 0rem;
  }
`

const Page = () => (
  <Layout>
    <Wrapper>
      <Helmet title={`Page not found | ${config.siteTitle}`} />
      <Header>
        <Link to="/">{config.siteTitle}</Link>
      </Header>
      <Content>
        <SectionTitle>Page not found</SectionTitle>
        <p>
          Try the <Link to="/">Home Page</Link> and <Link to="/categories">Categories</Link>.
        </p>
        <PlayThing />
      </Content>
    </Wrapper>
  </Layout>
)

export default Page
